#!/usr/bin/env python
# coding: utf-8

# # Neural Net Using S1 Waveforms 

# ## Author : Jordan Palmer
# 

# In[18]:


import uproot
import numpy as np
import pandas 
import matplotlib.pyplot as plt
import pandas as pd
import statistics


# #### Read in ER and NR pulse RQs defined in `waveformReadML.ipynb`

# In[19]:


df_ER_t = pd.read_pickle('ERrqs.pkl')
df_NR = pd.read_pickle('NRrqs.pkl')
df_ER = df_ER_t[:1761]


# #### Plot the prompt fraction for ER and NR 

# In[20]:


plt.hist(df_ER['prompt_fraction'], bins = 100, label='ER', alpha=0.5)
plt.xlabel('Prompt Fraction')
plt.hist(df_NR['prompt_fraction'], bins = 100, label='NR', alpha=0.5)
plt.axvline(statistics.median(df_NR['prompt_fraction']),color='k', linestyle='dashed',label='NR median')
plt.legend()
plt.show()


# #### Calculate the ER leakage into the right 50 % of the NR band

# In[21]:


totalERarea = np.sum(df_ER['prompt_fraction'])
print(totalERarea)
mediancut = df_ER['prompt_fraction'] > statistics.median(df_NR['prompt_fraction']) 
df_temp = pd.DataFrame()
df_temp['prompt_fraction'] = df_ER['prompt_fraction'][mediancut]
right50perc = np.sum(df_temp['prompt_fraction'])
leakage = right50perc/totalERarea
print(leakage)


# 
# #### Prepare inputs for NN
# ##### Combines ER and NR 

# In[75]:


variable_names = ['peak_heights','peak_positions','peak_prominences','peak_widths','prompt_fraction']
#variable_names = ['peak_widths','prompt_fraction', 'peak_heights','peak_prominences']
#variable_names = ['prompt_fraction','peak_positions']
df_ER['class']=0
df_NR['class']=1

df = df_ER.append(df_NR)
df = df.sample(frac=1).reset_index(drop=True) # Sort 
y_df = df.pop('class') #no cheating
df['answer']='ER'
df.loc[y_df==1,'answer']='NR'
df['answer'] = df['answer'].astype('category')

df = df.drop(['pulseArea_phd'], axis=1)
df


# ### Calculate sample weights

# In[76]:


(er_area, er_bins, er_patches) = plt.hist(df_ER['pulseArea_phd'],bins=10)
(nr_area, nr_bins, nr_patches) = plt.hist(df_NR['pulseArea_phd'],bins=10)
s1_bins = np.linspace(40,50,10)
s1_ind_er = np.digitize(df_ER['pulseArea_phd'], bins=s1_bins)
s1_ind_nr = np.digitize(df_NR['pulseArea_phd'], bins=s1_bins)

weight_er = 1/er_area[s1_ind_er]
weight_nr = 1/nr_area[s1_ind_nr]


# ### Plot Inputs

# In[77]:


import seaborn as sns
g = sns.pairplot(df, vars=variable_names, hue='answer', palette="Set2",height=3) # Diagonal plots are smoothed using kernel density estimator
g.savefig("ERNRRQcomparisons.png")


# In[78]:


vars_2 = ['peak_heights', 'peak_widths']
g = sns.pairplot(df, vars=vars_2, hue='answer', palette="Set2",height=3) # Diagonal plots are smoothed using kernel density estimator
vars_3 = ['peak_positions', 'peak_prominences']
g2 = sns.pairplot(df, vars=vars_3, hue='answer', palette="Set2",height=3) # Diagonal plots are smoothed using kernel density estimator


# ### Now, we have our ER and NR data merged together
# ### We can split into training and test sets

# In[79]:


from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.utils import class_weight
# Reduce to only the variables we want to train on, use numpy array format
X_unscaled = np.array(df[variable_names])
Y = np.array(y_df)
#ER_weight = df.size/df_ER.size #weight so that ER and NRs are treated equally
#NR_weight = df.size/df_NR.size

#weight_er = ER_weight*weight_er #absorb class weight in sample weight
#weight_nr = NR_weight*weight_nr
weights = np.append(weight_er,weight_nr)
#class_weight_ = {0: ER_weight, 1: NR_weight}
# Split data into training and testing (70/30 split); shuffle order, just in case
x_train_unscaled, x_test_unscaled, y_train, y_test, W_train, W_test = train_test_split(X_unscaled, Y, weights, test_size=0.1, random_state=0, shuffle=True)

# Perform initial transformations - StandardScaler sets mean of each feature to 0, std to 1
scaler = StandardScaler()
x_train = scaler.fit_transform(x_train_unscaled)
x_test = scaler.transform(x_test_unscaled) # Use same scaling transformation on test and train data (do not pick a new mean, std)
X = scaler.transform(X_unscaled) 


# ### Now, we can write a function to define our neural net architecture

# In[80]:


import tensorflow as tf
from tensorflow.keras.models import Sequential, clone_model
from tensorflow.keras.layers import Dense, Activation, Dropout
from tensorflow.keras import initializers as ki
from tensorflow.keras.metrics import AUC, Accuracy

#  Neural Network function which takes a topology vector (layer_vector)
#  Example of layer_vector; [1,4,1]  -  Network with one hidden layer of four nodes, one input and one output.
def neural_network( layer_vector ):
    model = Sequential() # The simplest networks like that shown above are all of the sequential type
    #  We add the first layer, and initialize the weights according to a Gaussian 
    model.add( Dense( layer_vector[1], input_dim=layer_vector[0], kernel_initializer=ki.RandomNormal(mean=0.0,stddev=1.0), use_bias=True ) )
    
    for i in range( 1, len( layer_vector ) - 1 ):
        #  This "layer" object applies the activation from the output of the previous
        model.add( Activation( 'tanh' ) )
        #  Adding the next layer
        model.add( Dense( layer_vector[i+1], kernel_initializer=ki.RandomNormal(mean=0.0,stddev=1.0), use_bias=True ) )
        
    model.add( Activation( 'sigmoid' ) )
    #  This compiles the network; the loss defines what we are trying to minimize by adjusting the network weights
    #  The optimizer decides the algorithm that does this minimization
    model.compile( loss= 'mean_squared_error', optimizer='Nadam', metrics=['accuracy',tf.keras.metrics.AUC(name='auc')]  )
    return model


# In[81]:


model = neural_network([5, 10, 3, 1])
model.summary()


# In[69]:


history = model.fit(x_train, y_train, validation_data=(x_test, y_test, W_test), epochs=50, batch_size=10, verbose=1, sample_weight=W_train)


# In[70]:


import seaborn as sns

y_pred_test = model.predict(x_test)
y_pred_train = model.predict(x_train)
y_pred_ER_train = model.predict(x_train[y_train==0])
y_pred_ER_test = model.predict(x_test[y_test==0])
y_pred_NR_train = model.predict(x_train[y_train==1])
y_pred_NR_test = model.predict(x_test[y_test==1])

n_bins=50
hist_range=(-0.1,1.1)
ER_color = sns.color_palette("Set2")[0]
NR_color = sns.color_palette("Set2")[1]
plt.figure(figsize=(10,5))
plt.hist(y_pred_ER_train,bins=n_bins,range=hist_range,density=True,alpha=0.4,color=ER_color,label="Train, ER")
counts,bin_edges = np.histogram(y_pred_ER_test,bins=n_bins,range=hist_range,density=True)
bin_centers = (bin_edges[:-1] + bin_edges[1:])/2.
plt.plot(bin_centers, counts,marker="o",linestyle="None",color=ER_color,label="Test, ER")

plt.hist(y_pred_NR_train,bins=n_bins,range=hist_range,density=True,alpha=0.4,color=NR_color,label="Train, NR")
counts,bin_edges = np.histogram(y_pred_NR_test,bins=n_bins,range=hist_range,density=True)
bin_centers = (bin_edges[:-1] + bin_edges[1:])/2.
plt.plot(bin_centers, counts,marker="o",linestyle="None",color=NR_color,label="Test, NR")

plt.legend()
plt.xlabel('NN output')
plt.yscale('log')


# In[71]:


plt.figure(1,figsize=(10,5))
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')

plt.figure(2,figsize=(10,5))
# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

plt.figure(3,figsize=(10,5))
# summarize history for loss
plt.plot(history.history['auc'])
plt.plot(history.history['val_auc'])
plt.title('model auc')
plt.ylabel('auc')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()


# In[72]:


from sklearn.metrics import roc_curve, roc_auc_score, auc, accuracy_score
import bisect
NN_FPR_test, NN_TPR_test, NN_TH_test = roc_curve(y_test, y_pred_test, pos_label=1) # Signal class set to 1

leakage_50 = NN_FPR_test[bisect.bisect_left(NN_TPR_test,0.5)]
print(leakage_50)

plt.plot(NN_FPR_test, NN_TPR_test, label="NN performance", color='orange')
plt.xlabel('Background leakage (FPR)')
plt.ylabel('Signal efficiency (TPR)')
#plt.xlim([0.1,0.2])
n_pts = 100
bkg_eff = np.linspace(0,1,n_pts)
sig_fixed = np.sqrt(bkg_eff)
#plt.plot(bkg_eff, sig_fixed, label="Const sig/sqrt(bkg)", color='black')
plt.legend()
plt.show()


# ### cross validation

# In[73]:


from sklearn.model_selection import cross_val_score, KFold, StratifiedKFold
from sklearn.base import clone
import copy
import bisect

# Helper function to do k-fold cross-validation 
# Note that sklearn does have a cross_val_score method, but you can't readily modify it to work with algorithms outside of sklearn
# and it also doesn't propagate weights correctly, so this is more general and works for weighted datasets
# X_, Y_, and W_ are the full (train+test) list of features, labels, and weights, respectively.
# model_ is the classifier to be trained
# cv is the cross validation object to use
# metrics is a list of metrics to evaluate on
# Returns a list of trained models (for each fold), a list of scores (for each metric and fold), and a list of indices used for the test partition (for each fold)
def k_fold_score_sklearn(X_, Y_,model_, cv, W_=None, verbose=False):
    if W_ is None: W_ = np.ones(np.shape(Y_))
    trained_models = []
    #scores = [[] for metric in metrics]
    train_partitions = []
    test_partitions = []
    scores = []
    # Split up data into different random partitions and train/test on each split
    k_ind = 0
    for train, test in cv.split(X_, Y_):
        train_partitions.append(train)
        test_partitions.append(test)
        # Clone model to store trained version later
        model_clone = clone_model(model_)
        #model_clone = copy.deepcopy(model_)  
        # Train model and make predictions on test data
        model_clone = model_
        #Y_pred_i = model_clone.fit(X_[train], Y_[train],sample_weight=W_[train]).predict(X_[test])
        model_clone.fit(X_[train], Y_[train], validation_data=(X_[test], Y_[test]), epochs=50, batch_size=10, verbose=0)
        Y_pred_i = model_clone.predict(X_[test])
        # Score on test data, then store results
        Y_test_i = (Y_[test]==1) # Make labels binary as desired by some score functions
        FPR, TPR, TH = roc_curve(Y_test_i, Y_pred_i, sample_weight=W_[test])
        leakage_50 = FPR[bisect.bisect_left(TPR,0.5)]
        
        scores.append(leakage_50)
        trained_models.append(model_clone)
        k_ind += 1
        if verbose: print("Model {0} of {1} trained".format(k_ind,cv.n_splits))
    #scores = np.array(scores)
    return (scores,trained_models, train_partitions, test_partitions)


# In[74]:


cv = StratifiedKFold(n_splits=10,shuffle=True)
model2 = neural_network([5, 10, 3, 1])
kfold, models, train_part, test_part = k_fold_score_sklearn(X, Y, model2,cv, W_=weights)
print(kfold)
print(np.mean(kfold))
print(np.std(kfold))


# In[57]:


model_int = 1
model = models[model_int]
x_train = X[train_part[model_int]]
y_train = Y[train_part[model_int]]
x_test = X[test_part[model_int]]
y_test = Y[test_part[model_int]]

y_pred_test = model.predict(x_test)
y_pred_train = model.predict(x_train)
y_pred_ER_train = model.predict(x_train[y_train==0])
y_pred_ER_test = model.predict(x_test[y_test==0])
y_pred_NR_train = model.predict(x_train[y_train==1])
y_pred_NR_test = model.predict(x_test[y_test==1])

n_bins=50
hist_range=(-0.1,1.1)
ER_color = sns.color_palette("Set2")[0]
NR_color = sns.color_palette("Set2")[1]
plt.figure(figsize=(10,5))
plt.hist(y_pred_ER_train,bins=n_bins,range=hist_range,density=True,alpha=0.4,color=ER_color,label="Train, ER")
counts,bin_edges = np.histogram(y_pred_ER_test,bins=n_bins,range=hist_range,density=True)
bin_centers = (bin_edges[:-1] + bin_edges[1:])/2.
plt.plot(bin_centers, counts,marker="o",linestyle="None",color=ER_color,label="Test, ER")

plt.hist(y_pred_NR_train,bins=n_bins,range=hist_range,density=True,alpha=0.4,color=NR_color,label="Train, NR")
counts,bin_edges = np.histogram(y_pred_NR_test,bins=n_bins,range=hist_range,density=True)
bin_centers = (bin_edges[:-1] + bin_edges[1:])/2.
plt.plot(bin_centers, counts,marker="o",linestyle="None",color=NR_color,label="Test, NR")

plt.legend()
plt.xlabel('NN output')
plt.yscale('log')

NN_FPR_test2, NN_TPR_test2, NN_TH_test2 = roc_curve(y_test, y_pred_test, pos_label=1) # Signal class set to 1

leakage_50_2 = NN_FPR_test2[bisect.bisect_left(NN_TPR_test2,0.5)]
print(leakage_50_2)


# In[19]:


test= np.array([0.15, 0.03, 0.06, 0.15, 0.12, 0.15, 0.15, 0.12, 0.03, 0.12])leakage_50_2


# In[20]:


np.std(test)


# In[ ]:





# In[ ]:





# In[ ]:




