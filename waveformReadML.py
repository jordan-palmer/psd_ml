#!/usr/bin/env python
# coding: utf-8

# # WaveformReadML
# 
# This script performs the following actions:<br>
# <ol>
# <li>Reads waveforms from LZap file<br>
# <li>Handles mutliple dimensions coexisting in a panda dataframe<br>
# <li>Selects S1 pulses only<br>
# <li>Normalises pulses to unit area<br>
# <li>Defines RQs : peak width, peak heights, peak prominence<br>
# <li>Defines AFT5 (time at an area of 5% of the total area of the pulse)<br>
# <li>Calculates the amplitude-weighted time of each pulse (new RQ)<br>
# <li>Defines prompt fraction<br>
# <li>Writes panda to a pickle file<br>
# </ol>
# 
# NOTE: throughout, the x axes are samples --> 1 sample = 10 ns
# 
# ### Author : Jordan Palmer (jordan.palmer.2014@live.rhul.ac.uk)
# #### Waveform reader from Ishan Khurana

# In[42]:


import uproot
import numpy as np
import pandas
import matplotlib.pyplot as plt
#import ray
#ray.init(num_cpus=4)
import pandas as pd
from multiprocessing import  Pool
#import coffea


# In[43]:


def get_pod_index(args):
    pod_samples = args[column_to_arg['pod_samples']]
    pod_start_times = args[column_to_arg['pod_start_times']] # Args correspond to the column. Second column in data df is pod_start_times
    pulse_start_times = args[column_to_arg['pulse_start_times']] # Third column in data df is pulse_start_times
    
    pod_times = [(j,j+ len(i)*10) for i,j in zip(pod_samples, pod_start_times)]    
    
    pod_index = {}
    new_index = 0
    
    # Match Pulse with Pod containing waveform
    for i,j in enumerate(pulse_start_times):
        pulse_found = False
        while not pulse_found:
            if pod_times[new_index][0]<=j and j<=pod_times[new_index][1]:
                pod_index[i] = (new_index)
                pulse_found=True
            else:
                new_index+=1
    
#     waveform = [pod_samples[i] for i in pod_index.values()]
#     pulse_class =[i.decode("utf-8") for i in pulse_classes]
    
    return pod_index


# In[44]:


def get_waveforms(args):
    pod_samples = args[column_to_arg['pod_samples']]
    pod_index = args[column_to_arg['pod_index']]
    pulse_pod_index = args[column_to_arg['pulse_pod_index']]
    pod_samples = [pod_samples[i] for i in pod_index.values()]
    pulse_waveform = [pod[pulse_pod_index[0]:pulse_pod_index[1]] for pod, pulse_pod_index in zip(pod_samples,pulse_pod_index)]
    return pulse_waveform


# In[45]:


def pulse_pod_index(args):
    pulse_start_time = args[column_to_arg['pulse_start_times']]
    pulse_end_time = args[column_to_arg['pulse_end_time']]    
    pod_indices = list(args[column_to_arg['pod_index']].values())
    
    pod_start_times= args[column_to_arg['pod_start_times']][pod_indices]
    
    pulse_start_index = (pulse_start_time-pod_start_times)/10
    pulse_end_index = (pulse_end_time-pod_start_times)/10
    
    pulse_start_index = [int(i) for i in pulse_start_index]
    pulse_end_index = [int(i) for i in pulse_end_index]
    
    return np.dstack([pulse_start_index,pulse_end_index])[0]


# #### 1. Read in the lzap file with waveforms saved and save all RQs necessary 

# In[46]:


get_ipython().run_cell_magic('time', '', 'erornr = \'ER\'\nfile_path = "/global/cfs/cdirs/lz/users/jpalmer/DATA/ERNRwaveforms/ER_output_sum.root"\ndata_file = uproot.open(file_path)\ndf = pd.DataFrame()\n\ndf[\'pod_samples\'] = data_file[\'PodWaveforms\'][b\'summedPodsTPCHG.podSamples\'].array()\ndf[\'eventID\'] = data_file[\'Events\'][b\'eventHeader.eventID\'].array()\ndf[\'pod_start_times\'] = data_file[\'PodWaveforms\'][b\'summedPodsTPCHG.podStartTime_ns\'].array()\ndf[\'pulse_end_time\'] = data_file[\'Events\'][b\'pulsesTPC.pulseEndTime_ns\'].array()\ndf[\'pulse_start_times\'] = data_file[\'Events\'][b\'pulsesTPC.pulseStartTime_ns\'].array()\ndf[\'pulse_s1\'] = data_file[\'Events\'][b\'pulsesTPC.s1Probability\'].array()\ndf[\'pulseArea_phd\'] = data_file[\'Events\'][b\'pulsesTPC.pulseArea_phd\'].array()\n#df[\'pulse_ID\'] = data_file[\'Events\'][b\'pulsesTPC.pulseID\'].array()\n#df[\'single_scatter_s1ID\'] = data_file[\'Scatters\'][b\'ss.s1PulseID\'].array()\n# The order in which the columns are added is needed for the functions to operate correctly. \ncolumn_to_arg = {\n    \'pod_samples\':0,\n    \'eventID\':1,\n    \'pod_start_times\':2,\n    \'pulse_end_time\':3,\n    \'pulse_start_times\':4,\n    \'pulse_s1\':5,\n    \'pulseArea_phd\':6,\n    #\'pulse_ID\':7,\n    #\'single_scatter_s1ID\':8,\n    \'pod_index\':7,\n    \'pulse_pod_index\':8,\n    \'pulse_waveform\':9\n}\n\ndf[\'pod_index\'] = df.apply(get_pod_index, axis =1)\ndf[\'pulse_pod_index\'] = df.apply(pulse_pod_index,axis = 1)\ndf[\'pulse_waveform\'] = df.apply(get_waveforms, axis =1)')


# ##### This cell just gives each row a second index. This number corresponds to the waveform viewer "event"

# In[47]:


#df = df[0:500]
index_original =[]
for i in range(df.shape[0]):
    index_original.append(i)
df['index_original'] = index_original
print(df.shape[0])


# #### 2. Deal with multiple dimensions
# #### this enables me to perform pulse-level cuts

# In[48]:


get_ipython().run_cell_magic('time', '', '#cut = df[\'pulseArea_phd\'] > 5\nfrom pandas.api.types import is_object_dtype\n\n_explodable_types = (tuple, list, np.ndarray)\n\ndef explode(df):\n    """\n    Based on this answer:\n    https://stackoverflow.com/questions/12680754/split-explode-pandas\\\n    -dataframe-string-entry-to-separate-rows/40449726#40449726\n    \n    Also inside FAST-LZ (fast carpenter) CREDIT : Ben Krikler\n    """\n    # get the list columns\n    lst_cols = [col for col, dtype in df.dtypes.items() if is_object_dtype(dtype)]\n    # Be more specific about which objects are ok\n    lst_cols = [col for col in lst_cols if isinstance(df[col][0], _explodable_types)]\n    if not lst_cols:\n        return df\n\n    # all columns except `lst_cols`\n    idx_cols = df.columns.difference(lst_cols)\n\n    # check all lists have same length\n    lens = pd.DataFrame({col: df[col].str.len() for col in lst_cols})\n    print(lens)\n    different_length = (lens.nunique(axis=1) > 1).any()\n    if different_length:\n        raise ValueError("Cannot bin multiple arrays with different jaggedness")\n    lens = lens[lst_cols[0]]\n    \n    # create "exploded" DF\n    flattened = {col: df.loc[lens > 0, col].values for col in lst_cols}\n    flattened = {col: sum(map(list, vals), []) for col, vals in flattened.items()}\n    res = pd.DataFrame({col: np.repeat(df[col].values, lens) for col in idx_cols})\n    res = res.assign(**flattened)\n\n    # Check that rows are fully "exploded"\n    return res #to explode > 1 time, return explode(res)\n\n#drop the pod samples and start times (not currently being used from now on)\n')


# ##### Drop all columns you dont need, this part can take some time

# In[49]:


get_ipython().run_cell_magic('time', '', "df_drop = df.drop(['pod_samples', 'pod_start_times','eventID'], axis=1)\n#P = Pool(processes=20)\n#result = P.map_async(explode, df_drop)\ndf_explode = explode(df_drop) #sort out dimensionality in order to perform pulse level cuts")


# ##### Reset the index every time a cut has been made so you can easily loop over the dataframe

# In[50]:



df_explode= df_explode.reset_index(drop=True)
pulseareacut = df_explode['pulseArea_phd'] > 40 
pulseareacutupper = df_explode['pulseArea_phd'] <50
df_final = df_explode[pulseareacut&pulseareacutupper]


# #### 3. Select S1 pulses and first S1
# ##### OD people can skip this step

# In[51]:



df_final['pulse_s1']
s1s = df_final['pulse_s1']
df_final = df_final[df_final['pulse_s1']==1]


# In[52]:


#df_final = df_final.groupby('index_original').head(1)
df_final


# #### 4. Normalise Waveforms to Unit Area
# ##### This step is important if you dont want the ML algorithm to pick up on the pulse's area. Think about if this is what you want

# In[54]:


import numpy as np
from sklearn.preprocessing import normalize

def normalise_array(x):
    x = np.asarray(x)
    norm2 = x / np.sum(x,keepdims=True) 
    return norm2

df_final['pulse_waveform_norm'] = df_final.apply(lambda row : normalise_array(row['pulse_waveform']), axis = 1)
df_final = df_final.reset_index(drop=True)


# #### 5. Define RQs like prompt fraction, peak position, peak height, peak width, and peak prominence

# In[69]:


get_ipython().run_cell_magic('capture', '', '"""\nDefining peak RQs for ML. \nProbably need to tune the threshold \n"""\nfrom scipy.signal import chirp,find_peaks, peak_widths, peak_prominences\nimport matplotlib.backends.backend_pdf\n\n\ndef find_peaks_(x):\n    peaks, _ = find_peaks(x, height=0.02)\n    return peaks\ndef find_peak_heights(x):\n    peaks, h = find_peaks(x, height=0.02)\n    return h["peak_heights"]\ndef find_peak_widths(x):\n    peaks, h = find_peaks(x, height=0.02)\n    widths = peak_widths(x,peaks)\n    return widths[0]\ndef find_peak_prominence(x):\n    peaks, h = find_peaks(x, height=0.02)\n    prominence = peak_prominences(x, peaks)\n    return prominence[0]\n\npeak_positions_ = []\npeak_heights_ = []\npeak_widths_ = []\npeak_prominences_ = []\npulse_area = []\n#eventID=[]\noriginal_index2 =[]\nfor i in range(df_final[\'pulse_waveform_norm\'].shape[0]):\n    peak_positions_.append(find_peaks_(df_final[\'pulse_waveform_norm\'][i]))\n    peak_heights_.append(find_peak_heights(df_final[\'pulse_waveform_norm\'][i]))\n    peak_widths_.append(find_peak_widths(df_final[\'pulse_waveform_norm\'][i]))\n    peak_prominences_.append(find_peak_prominence(df_final[\'pulse_waveform_norm\'][i]))\n    pulse_area.append(df_final[\'pulseArea_phd\'][i])\n    #eventID.append(df_final[\'eventID\'][i])\n    original_index2.append(df_final[\'index_original\'][i])\n\npeak_df = pd.DataFrame()\npeak_df[\'peak_heights\'] = np.asarray(peak_heights_)\npeak_df[\'peak_positions\'] = np.asarray(peak_positions_)\npeak_df[\'peak_prominences\'] = np.asarray(peak_prominences_)\npeak_df[\'peak_widths\'] = np.asarray(peak_widths_)\npeak_df[\'peak_waveforms\'] = np.asarray(df_final[\'pulse_waveform_norm\'])\npeak_df[\'pulseArea_phd\'] = np.asarray(pulse_area)\n#peak_df[\'eventID\'] = np.array(eventID)\npeak_df[\'original_index\'] = np.array(original_index2)\n# select waveforms with a reasonable number of peaks and waveforms that dont start too high \nfor i in range(peak_df[\'peak_positions\'].shape[0]):\n    if (peak_df[\'peak_positions\'][i].shape[0] > 2) | (peak_df[\'peak_waveforms\'][i][0] >= 0.03) | (peak_df[\'peak_waveforms\'][i][peak_df[\'peak_waveforms\'][i].shape[0]-1] >= 0.025):  \n        peak_df =peak_df.drop(i)\n\n\n\npeak_df= peak_df.reset_index(drop=True)\n\nprint(max(peak_df[\'peak_prominences\'][0]))\n\nfinal_positions = []\nfinal_heights = []\nfinal_widths = []\nfinal_prominances = []\nfinal_waveforms = []\nfinal_areas = []\n#final_eventID = []\nfinal_index = []\nfor i in range(peak_df[\'peak_prominences\'].shape[0]):\n    for j in range(peak_df[\'peak_prominences\'][i].shape[0]):\n        if peak_df[\'peak_prominences\'][i][j] == max(peak_df[\'peak_prominences\'][i]):\n            final_prominances.append(peak_df[\'peak_prominences\'][i][j])\n            final_positions.append(peak_df[\'peak_positions\'][i][j])\n            final_widths.append(peak_df[\'peak_widths\'][i][j])\n            final_heights.append(peak_df[\'peak_heights\'][i][j])\n            final_areas.append(peak_df[\'pulseArea_phd\'][i])\n            final_waveforms.append(peak_df[\'peak_waveforms\'][i])\n            #final_eventID.append(peak_df[\'eventID\'][i])\n            final_index.append(peak_df[\'original_index\'][i])\n\npeak_df_f = pd.DataFrame()\npeak_df_f[\'peak_heights\'] = np.asarray(final_heights)\npeak_df_f[\'peak_positions\'] = np.asarray(final_positions)\npeak_df_f[\'peak_prominences\'] = np.asarray(final_prominances)\npeak_df_f[\'peak_widths\'] = np.asarray(final_widths)\npeak_df_f[\'pulseArea_phd\'] = np.asarray(final_areas)\npeak_df_f[\'peak_waveforms\'] = np.asarray(final_waveforms)\n#peak_df_f[\'eventID\'] = np.asarray(final_eventID)\npeak_df_f[\'original_index\'] = np.asarray(final_index)')


# #### 6. Finding the 5% AFT

# In[70]:


from scipy import integrate

AFT5 = []
for i in range(peak_df['peak_waveforms'].shape[0]):
    AFT5_temp = [] 
    accum = np.cumsum(peak_df['peak_waveforms'][i])
    for j in range(accum.shape[0]):
        if accum[j] >= 0.05:
            AFT5_temp.append(j)
    AFT5.append(AFT5_temp[0])
peak_df_f['AFT5'] = AFT5

peak_df_f['rel_peak_positions'] = peak_df_f['peak_positions'] - peak_df_f['AFT5'] #peak position wrt AFT5
peak_df_f.drop('peak_positions',1)


# #### 7. Find the amplitude-weighted average time wrt t0
# ##### Suggestion from Dan Tovey as a PSD RQ

# In[71]:


#(1/(\sum_i a_i))*\sum_i ((t_i-t_0) * a_i)

amp_weighted_time = []

for i in range(peak_df_f['peak_waveforms'].shape[0]): #loop over each waveform
    first_index = peak_df_f['rel_peak_positions'][i] #take the first index to be the peak time
    tot_area = np.sum(peak_df_f['peak_waveforms'][i][first_index:50]) #area from t_0 to t_i 
    amp_weight_sum_tmp = 0
    rolling_area = 0
    for j in range(peak_df_f['peak_waveforms'][i].shape[0]): #loop over waveform i
        if first_index+j >= peak_df_f['peak_waveforms'][i].shape[0]:
            break
        #+= the amp weighted time 
        amp_weight_sum_tmp = amp_weight_sum_tmp + (1/tot_area)*(((first_index+j) - first_index)*peak_df_f['peak_waveforms'][i][first_index+j])

    amp_weighted_time.append(amp_weight_sum_tmp)

    

peak_df_f['amp_weighted_time'] = np.asarray(amp_weighted_time)
peak_df_f


# #### 8. Prompt Fraction Definition
# ##### OD people will need to change these boundaries

# In[72]:


"""
Next, calculate the prompt fraction of each pulse. In LUX, this integral was from -8 ns to 32 ns/−14 to 134 ns.
"""
from scipy import integrate
promptfraction = []
for i in range(peak_df_f['peak_waveforms'].shape[0]):
    #print(np.sum(peak_df['peak_waveforms'][i][0:32]))
    #print(np.sum(peak_df['peak_waveforms'][i][0:150]))    
    lower_lim = peak_df_f['AFT5'][i]
    promptfraction.append((np.sum(peak_df_f['peak_waveforms'][i][lower_lim:lower_lim+12])/np.sum(peak_df_f['peak_waveforms'][i][lower_lim:lower_lim+50])))

peak_df_f['prompt_fraction'] = promptfraction
plt.plot(peak_df['peak_waveforms'][3])


# #### 9. Write Panda to Pickle
# ##### Just because CSV write things as strings and it got annoying 

# In[17]:


peak_df_f.to_pickle('/global/project/projectdirs/lz/users/jpalmer/PSD_ML/'+erornr+'rqs.pkl')


# ##### Show the final s1 waveforms

# In[3]:


#for i in range(peak_df_f.shape[0]):
#    plt.plot(peak_df_f['peak_waveforms'][i])
#    print(peak_df_f['peak_waveforms'][i])
#    plt.show()


# In[ ]:




