The numbers here correspond to the chapter numbers in each python file<br>

# WaveformReadML

This script performs the following actions:<br>
<ol>
<li>Reads waveforms from LZap file<br>
<li>Handles mutliple dimensions coexisting in a panda dataframe<br>
<li>Selects S1 pulses only<br>
<li>Normalises pulses to unit area<br>
<li>Defines RQs : peak width, peak heights, peak prominence<br>
<li>Defines AFT5 (time at an area of 5% of the total area of the pulse)<br>
<li>Calculates the amplitude-weighted time of each pulse (new RQ)<br>
<li>Defines prompt fraction<br>
<li>Writes panda to a pickle file<br>
</ol>

# PSD Using Convolutional Neural Nets 
#### This script performs the following actions:
<ol>
<li>Read ER and NR pulses selected in waveformReadML.ipynb <br>
<li>Calculate AFT5 and select the time window from AFT - 3 samples to 50 samples (zero-pad if necessary) <br>
<li>Calculate S1 sample weighting and split into train and test data <br>
<li>A function is defined for producing the CNN 
<li>An instance of this function is called and the CNN model is compiled
<li>The CNN output is plotted as well as the ROC curve. Also, the model loss vs epoch is plotted
<li>Kfold cross validation is performed to test CNN
<li>A grid search is performed to find optimal parameters for the CNN
</ol>


# PSD Using Neural Nets 
#### This script performs the following actions:
<ol>
<li>Read ER and NR pulses selected in waveformReadML.ipynb <br>
<li>The prompt fraction is plotted and the leakage is calculated
<li>The inputs are prepared for the NN
<li>Sample weights are calculated to ensure the NN doesn't pick up on any non-flattness in S1 size
<li>The input RQs are visualised to get an initial idea of ER/NR separation
<li>Split into train/test data sets
<li>Define NN function
<li> Make a NN model, compile, and fit
<li>Plot NN output and ROC curve
<li>Kfold cross validation to check performance of NN
<li>Code to help you test a particular model produced in the cross validation
</ol>



