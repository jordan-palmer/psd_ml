#!/usr/bin/env python
# coding: utf-8

# In[70]:


import uproot
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pandas as pd
import statistics


# ## Read Waveforms

# In[71]:


df_NR_t = pd.read_pickle('NRrqs.pkl')
df_ER = pd.read_pickle('ERrqs.pkl')
df_NR = df_NR_t[:1419]
print(df_ER.shape[0])
print(df_NR.shape[0])
print(df_NR['peak_waveforms'])


# ## Standardise Waveforms
# ### Take the waveform from a few samples before the AFT5 

# ### AFT5

# In[72]:


from scipy import integrate

def getAFT5(waveforms):
    AFT5_temp = []
    accum = np.cumsum(waveforms)
    for j in range(accum.shape[0]):
        if accum[j] >= 0.05:
            AFT5_temp.append(j)
    AFT5= AFT5_temp[0]
    new_waveform = waveforms[AFT5-3:]
    padding = 50 - len(new_waveform) #each one 50 samples 
    if padding > 0:
        new_waveform = np.pad(new_waveform, (0, padding), 'constant')
    elif padding < 0:
        new_waveform=new_waveform[:50]
    return new_waveform
        


# In[73]:


final_waveforms_ER = []
final_waveforms_NR = []

for i in range(df_ER['peak_waveforms'].shape[0]):
    final_waveforms_ER.append(getAFT5(df_ER['peak_waveforms'][i]))

for i in range(df_NR['peak_waveforms'].shape[0]):
    final_waveforms_NR.append(getAFT5(df_NR['peak_waveforms'][i]))

df_ER['padded_waveforms'] = final_waveforms_ER
df_NR['padded_waveforms'] = final_waveforms_NR


# In[74]:


waveforms_ER = pd.DataFrame()
waveforms_NR = pd.DataFrame()

waveforms_ER['padded_waveforms'] = np.asarray(df_ER['padded_waveforms'])
waveforms_NR['padded_waveforms'] = np.asarray(df_NR['padded_waveforms'])
waveforms_ER['class']=0
waveforms_NR['class']=1


# In[75]:


df = waveforms_ER.append(waveforms_NR)
df = df.sample(frac=1).reset_index(drop=True) # Sort 
y_df = df.pop('class')
df['answer']='ER'
df.loc[y_df==1,'answer']='NR'
df['answer'] = df['answer'].astype('category')
df


# ### Weighting

# In[76]:


from sklearn.model_selection import train_test_split

(er_area, er_bins, er_patches) = plt.hist(df_ER['pulseArea_phd'],bins=10)
(nr_area, nr_bins, nr_patches) = plt.hist(df_NR['pulseArea_phd'],bins=10)
s1_bins = np.linspace(40,50,10)
s1_ind_er = np.digitize(df_ER['pulseArea_phd'], bins=s1_bins)
s1_ind_nr = np.digitize(df_NR['pulseArea_phd'], bins=s1_bins)

weight_er = 1/er_area[s1_ind_er]
weight_nr = 1/nr_area[s1_ind_nr]

#ER_weight = df.size/df_ER.size #weight so that ER and NRs are treated equally
#NR_weight = df.size/df_NR.size

#weight_er = ER_weight*weight_er #absorb class weight in sample weight
#weight_nr = NR_weight*weight_nr
weights = np.append(weight_er,weight_nr)

X=np.stack(df['padded_waveforms'].values, axis=0)
Y=np.array(y_df)
#class_weight_ = {0: ER_weight, 1: NR_weight}
# Split data into training and testing (70/30 split); shuffle order, just in case
x_train_unscaled, x_test_unscaled, y_train, y_test, W_train, W_test = train_test_split(X, Y, weights, test_size=0.1, random_state=0, shuffle=True)

#Reshape for keras
x_train_unscaled=x_train_unscaled.reshape(x_train_unscaled.shape[0],50,1)
x_test_unscaled=x_test_unscaled.reshape(x_test_unscaled.shape[0],50,1)

#x_train_unscaled.reshape(x_train_unscaled.shape[0], 50, 1)


# ### CNN function

# In[77]:


import tensorflow.keras.layers
import tensorflow.keras.models
import tensorflow.keras.regularizers
import tensorflow.keras.optimizers
import keras
from tensorflow.keras.callbacks import LearningRateScheduler
from tensorflow.keras import backend as K

def generate_fcn(SEQUENCE_LENGTH, FILT_NUM, FILT_SIZE):
    """
    Generates a fully convolutional neural network to measure the widths of square pulses.

    param: SEQUENCE_LENGTH - the length (in samples) of the the square wave pulses
    param: FILT_NUM - the number of convolutional filters
    param: FILT_SIZE - the length of the FILT_NUM convolutional filters

    return: myModel - a single layer FCN

    """ 
    # The input to the model
    ip = keras.layers.Input(shape = (SEQUENCE_LENGTH,1))
    
    # Direct the flow of the input tensor
    y = ip

    ## Convolutional block, 
    # y is the input to each new layer and
    # y stores the new output from each call
    y = keras.layers.Conv1D(
    filters = FILT_NUM, 
    kernel_size = FILT_SIZE, 
    strides = 1, 
    padding = 'valid',
    kernel_initializer = 'he_uniform'
    )(y)

    #y = keras.layers.BatchNormalization()(y) # There's a bug with keras/Batch Normalization right now. setting training = True mitigates that
    y = keras.layers.Activation('relu')(y)

    # The Global Pooling layer, which reduces each feature map to a single value
    y = keras.layers.GlobalAveragePooling1D()(y)   

    # Defines the output layer of the model
    #y = keras.layers.Flatten()(y)  #put back if pooling is removed
    out = keras.layers.Dense(units = 1, 
    activation = 'sigmoid')(y)

    # Creates the model under keras' sequential API
    myModel = keras.models.Model(ip, out)
    myModel.summary()
    return myModel


# In[78]:


fcn_model = generate_fcn(SEQUENCE_LENGTH = 50,
                         FILT_NUM = 20, 
                         FILT_SIZE = 10
                         )


# In[79]:


fcn_model.compile


# In[80]:


fcn_model.compile(optimizer = keras.optimizers.Adam(),
                  loss = 'mean_squared_error', 
                  
                  )


# In[81]:


fcn_model.summary()


# In[ ]:


get_ipython().run_cell_magic('capture', '', 'training_history = fcn_model.fit(\n    # Training Pulses\n    x = x_train_unscaled, # data to train on\n    y = y_train, # labels of training data\n    # Validation Pulses Information - slightly different format :-(\n    validation_data = (x_test_unscaled, y_test, W_test), # (validation data, validation labels)\n    #batch_size = bSize, # Batch size sets the umber of pulses sent through the network before weights are adjusted\n    epochs = 500, # number of epochs to train\n    verbose = 1, # setting to 1 prints training progress\n    sample_weight=W_train\n    #callbacks = LR_callback, # keras callbacks are used for making network adjustments during training\n    ) ')


# In[ ]:


testing_predictions=fcn_model.predict(x_test_unscaled)
NR= testing_predictions[y_test==1]
ER=testing_predictions[y_test==0] 


# In[ ]:


plt.hist(NR, bins=20, label='NR')
plt.hist(ER, bins=20, label='ER')
plt.legend()


# ### How well did we do?

# In[ ]:


import seaborn as sns

y_pred_test = fcn_model.predict(x_test_unscaled)
y_pred_train = fcn_model.predict(x_train_unscaled)
y_pred_ER_train = fcn_model.predict(x_train_unscaled[y_train==0])
y_pred_ER_test = fcn_model.predict(x_test_unscaled[y_test==0])
y_pred_NR_train = fcn_model.predict(x_train_unscaled[y_train==1])
y_pred_NR_test = fcn_model.predict(x_test_unscaled[y_test==1])

n_bins=50
hist_range=(-0.1,1.1)
ER_color = sns.color_palette("Set2")[0]
NR_color = sns.color_palette("Set2")[1]
plt.figure(figsize=(10,5))
plt.hist(y_pred_ER_train,bins=n_bins,range=hist_range,density=True,alpha=0.4,color=ER_color,label="Train, ER")
counts,bin_edges = np.histogram(y_pred_ER_test,bins=n_bins,range=hist_range,density=True)
bin_centers = (bin_edges[:-1] + bin_edges[1:])/2.
plt.plot(bin_centers, counts,marker="o",linestyle="None",color=ER_color,label="Test, ER")

plt.hist(y_pred_NR_train,bins=n_bins,range=hist_range,density=True,alpha=0.4,color=NR_color,label="Train, NR")
counts,bin_edges = np.histogram(y_pred_NR_test,bins=n_bins,range=hist_range,density=True)
bin_centers = (bin_edges[:-1] + bin_edges[1:])/2.
plt.plot(bin_centers, counts,marker="o",linestyle="None",color=NR_color,label="Test, NR")

plt.legend()
plt.xlabel('NN output')
plt.yscale('log')


# In[ ]:


from sklearn.metrics import roc_curve, roc_auc_score, auc, accuracy_score
import bisect
NN_FPR_test, NN_TPR_test, NN_TH_test = roc_curve(y_test, y_pred_test, pos_label=1) # Signal class set to 1
leakage_50 = NN_FPR_test[bisect.bisect_left(NN_TPR_test,0.5)]
print(leakage_50)
f = plt.plot(NN_FPR_test, NN_TPR_test, label="NN performance", color='orange')
plt.xlabel('Background leakage (FPR)')
plt.ylabel('Signal efficiency (TPR)')

n_pts = 100
bkg_eff = np.linspace(0,1,n_pts)
sig_fixed = np.sqrt(bkg_eff)
#plt.plot(bkg_eff, sig_fixed, label="Const sig/sqrt(bkg)", color='black')
plt.legend()
plt.show()


# In[ ]:




plt.figure(2,figsize=(10,5))
# summarize history for loss
plt.plot(training_history.history['loss'])
plt.plot(training_history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.xlim([0,500])
plt.legend(['train', 'test'], loc='upper left')
plt.show()


# ### Kfold cross validation

# In[ ]:


from sklearn.model_selection import cross_val_score, KFold, StratifiedKFold
from sklearn.base import clone
import copy
import bisect
from tensorflow.keras.models import Sequential, clone_model

# Helper function to do k-fold cross-validation 
# Note that sklearn does have a cross_val_score method, but you can't readily modify it to work with algorithms outside of sklearn
# and it also doesn't propagate weights correctly, so this is more general and works for weighted datasets
# X_, Y_, and W_ are the full (train+test) list of features, labels, and weights, respectively.
# model_ is the classifier to be trained
# cv is the cross validation object to use
# metrics is a list of metrics to evaluate on
# Returns a list of trained models (for each fold), a list of scores (for each metric and fold), and a list of indices used for the test partition (for each fold)
def k_fold_score_sklearn(X_, Y_,model_, cv, W_=None, verbose=False):
    if W_ is None: W_ = np.ones(np.shape(Y_))
    trained_models = []
    #scores = [[] for metric in metrics]
    train_partitions = []
    test_partitions = []
    scores = []
    # Split up data into different random partitions and train/test on each split
    k_ind = 0
    for train, test in cv.split(X_, Y_):
        train_partitions.append(train)
        test_partitions.append(test)
        # Clone model to store trained version later
        #model_clone = clone_model(model_)
        model_clone = copy.deepcopy(model_)  
        # Train model and make predictions on test data
        #model_clone = model_
        #Y_pred_i = model_clone.fit(X_[train], Y_[train],sample_weight=W_[train]).predict(X_[test])
        
        training_history = model_.fit(
            # Training Pulses
            x = X_[train], # data to train on
            y = Y_[train], # labels of training data
            # Validation Pulses Information - slightly different format :-(
            validation_data = (X_[test], Y_[test], W_[test]), # (validation data, validation labels)
            #batch_size = bSize, # Batch size sets the umber of pulses sent through the network before weights are adjusted
            epochs = 500, # number of epochs to train
            verbose = 1, # setting to 1 prints training progress
            sample_weight=W_[train]
            #callbacks = LR_callback, # keras callbacks are used for making network adjustments during training
            ) 
        #model_clone.fit(X_[train], Y_[train], validation_data=(X_[test], Y_[test]), epochs=50, batch_size=10, verbose=0)
        Y_pred_i = model_clone.predict(X_[test])
        # Score on test data, then store results
        Y_test_i = (Y_[test]==1) # Make labels binary as desired by some score functions
        FPR, TPR, TH = roc_curve(Y_test_i, Y_pred_i, sample_weight=W_[test])
        leakage_50 = FPR[bisect.bisect_left(TPR,0.5)]
        
        scores.append(leakage_50)
        #trained_models.append(model_clone)
        k_ind += 1
        if verbose: print("Model {0} of {1} trained".format(k_ind,cv.n_splits))
    #scores = np.array(scores)
    return (scores,train_partitions, test_partitions)


# In[ ]:


get_ipython().run_cell_magic('capture', '', "cv = StratifiedKFold(n_splits=10,shuffle=True)\nfcn_model_kfold = generate_fcn(SEQUENCE_LENGTH = 50,\n                         FILT_NUM = 20, \n                         FILT_SIZE = 10\n                         )\nX = X.reshape(X.shape[0], 50, 1)\n\nfcn_model_kfold.compile(optimizer = keras.optimizers.Adam(),\n                  loss = 'mean_squared_error', \n                  )\nkfold, train_part, test_part = k_fold_score_sklearn(X, Y, fcn_model_kfold,cv, W_=weights)")


# In[ ]:


print(kfold)
print(np.mean(kfold))
print(np.std(kfold))


# In[ ]:




